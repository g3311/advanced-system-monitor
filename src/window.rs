use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/g3311/advanced-system-monitor/window.ui")]
    pub struct AdvancedSystemMonitorWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AdvancedSystemMonitorWindow {
        const NAME: &'static str = "AdvancedSystemMonitorWindow";
        type Type = super::AdvancedSystemMonitorWindow;
        type ParentType = gtk::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AdvancedSystemMonitorWindow {}
    impl WidgetImpl for AdvancedSystemMonitorWindow {}
    impl WindowImpl for AdvancedSystemMonitorWindow {}
    impl ApplicationWindowImpl for AdvancedSystemMonitorWindow {}
}

glib::wrapper! {
    pub struct AdvancedSystemMonitorWindow(ObjectSubclass<imp::AdvancedSystemMonitorWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl AdvancedSystemMonitorWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::new(&[("application", application)])
            .expect("Failed to create AdvancedSystemMonitorWindow")
    }
}
